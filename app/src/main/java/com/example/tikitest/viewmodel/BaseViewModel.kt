package com.example.tikitest.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tikitest.model.KeywordModel

open class BaseViewModel : ViewModel() {
    val eventLoading = MutableLiveData<Boolean>()
    val eventFailure = MutableLiveData<Throwable>()
    val eventSuccess = MutableLiveData<List<KeywordModel>>()

    fun showLoading(value: Boolean) {
        eventLoading.value = value
    }

    fun showFailure(throwable: Throwable) {
        eventFailure.value = throwable
    }

    fun showSuccessful(list: ArrayList<KeywordModel>) {
        eventSuccess.value = list

    }
}

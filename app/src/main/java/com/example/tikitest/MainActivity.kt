package com.example.tikitest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import com.example.tikitest.adapter.KeywordAdapter
import com.example.tikitest.viewmodel.KeywordViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViewModel()
    }

    private fun setupViewModel() {
        val viewModel = ViewModelProviders.of(this).get(KeywordViewModel::class.java)
        viewModel.loadKeyword()
        setObserveLive(viewModel)
    }

    private fun setObserveLive(viewModel: KeywordViewModel) {
        viewModel.eventSuccess.observe(this, Observer {listKeyword ->
            recyclerView.apply {
                layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
                adapter = listKeyword?.let { KeywordAdapter(it) }
            }
        })

        viewModel.eventLoading.observe(this, Observer { isShow ->
            progress_loading.visibility = if (isShow == true) View.VISIBLE else View.GONE
        })

        viewModel.eventFailure.observe(this, Observer {error->
            text_error.visibility = View.VISIBLE
            text_error.text = error?.message
        })
    }
}
